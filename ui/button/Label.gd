extends Label


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_mouse_entered():
	add_color_override("font_color", Color(0.7,0.7,0.7))
	pass # Replace with function body.


func _on_mouse_exited():
	add_color_override("font_color", Color(1,1,1))
	pass # Replace with function body.
