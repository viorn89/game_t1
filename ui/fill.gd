extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	set_size(get_viewport_rect().size)
	get_tree().get_root().connect("size_changed", self, "size_changed")
	pass # Replace with function body.

func size_changed():
	set_size(get_viewport_rect().size)
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
