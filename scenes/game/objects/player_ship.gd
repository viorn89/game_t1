extends KinematicBody2D

export var max_speed = 160.0
export var deceleration = 0.5
export var acceleration = 5.0
export var max_rotate_speed = 3.0
export var rotate_deceleration = 0.03
export var rotate_acceleration = 0.1
var speed = 0.0
var rotate_speed = 0.0


# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("main_engine").hide()
	get_node("left_top").hide()
	get_node("right_top").hide()
	get_node("left_bottom").hide()
	get_node("right_bottom").hide()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("ui_up"):
		get_node("main_engine").show()
		if speed<max_speed:
			speed+=acceleration
		if speed>max_speed: 
			speed = max_speed
	else:
		get_node("main_engine").hide()
	if Input.is_action_pressed("ui_left"):
		get_node("right_top").show()
		get_node("left_bottom").show()
		if rotate_speed<max_rotate_speed:
			rotate_speed+=rotate_acceleration
		if rotate_speed>max_rotate_speed: 
			rotate_speed = max_rotate_speed
		
	else:
		get_node("right_top").hide()
		get_node("left_bottom").hide()
		
	if Input.is_action_pressed("ui_right"):
		get_node("left_top").show()
		get_node("right_bottom").show()
		if rotate_speed>-max_rotate_speed:
			rotate_speed-=rotate_acceleration
		if rotate_speed<-max_rotate_speed:
			rotate_speed = -max_rotate_speed
		
	else:
		get_node("left_top").hide()
		get_node("right_bottom").hide()
		
	
	if rotate_speed>0:
		rotate_speed-=rotate_deceleration
	if rotate_speed<0:
		rotate_speed+=rotate_deceleration
	if speed>0:
		speed-=deceleration
	
	
	rotation -= rotate_speed*delta
	move_and_collide(Vector2.UP.rotated(rotation) * speed * delta)
