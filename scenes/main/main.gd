extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().get_root().connect("size_changed", self, "size_changed")
	print("hello!")
	pass

func size_changed():
	print("size: ",  get_viewport_rect().size)
	pass
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_StartBtn_pressed():
	get_tree().change_scene("res://scenes/game/game.tscn")
	pass # Replace with function body.


func _on_SettingBtn_pressed():
	pass # Replace with function body.


func _on_ExtBtn_pressed():
	get_tree().quit()
	pass # Replace with function body.
